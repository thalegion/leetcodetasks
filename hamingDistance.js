/**
 * @param {number} x
 * @param {number} y
 * @return {number}
 */
var hammingDistance = function(x, y) {
    var bin_x = (x >>> 0).toString(2),
        bin_y = (y >>> 0).toString(2),
        len_diff = Math.abs(bin_x.length - bin_y.length);
    
    if (bin_x.length > bin_y.length)
        bin_y = "0".repeat(len_diff) + bin_y;
    else if (bin_x.length < bin_y.length)
        bin_x = "0".repeat(len_diff) + bin_x;
    
    var diff_count = 0;
    
    bin_x.split('').forEach(function(el, key) {
       if (el != bin_y[key])
           diff_count++;
    });
    
    return diff_count;
};

