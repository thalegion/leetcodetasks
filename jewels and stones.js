
/**
* @param {string} J
* @param {string} S
* @return {number}
*/
var numJewelsInStones = function(J, S) {
    var num = 0;
    S.split('').forEach(function(char){
        if (J.includes(char)) {
            num++;   
        }
    });

    return num;
};